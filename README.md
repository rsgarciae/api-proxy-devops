# api-proxy-devops

Recipe app API proxy application from DevOps Deployment Automation with Terraform, AWS and Docker
NGNIX proxy app for a recipe app API

##Usage

### Environment Variables 
* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward request to (default: `app`)
* `APP_PORT` - Port of the app to forward request to (default: `9000`)
